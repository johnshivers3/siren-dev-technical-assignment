const peoplePool = require("./people_pool.json");

/*
Siren technical interview - Coding assignment

Problem:

At Facebook, you have friends who all have a list of movies that they like.
Write a function that returns the most popular movie of a person in his/her network of friends.
Network means that you have to consider friends of friends as well.

Solution:

Use object oriented design with the language you like. Write at least 1 test for the happy path.

/*
  class Person {
    constructor(name, email) {
      this.name = name;
      this.email = email;
      this.network = [];
      this.movies = Array(3);
    }
  }
*/
const getMostPopMovie = (person) => {
  // Create object to document number of occurrences of each movie in the network.
  const movieOccurrences = {};
  // Create Set object to ensure friends are visited only once.
  const visited = new Set();
  /*
  Create Array object to enqueue friends for iteration.
  Object begins with the person argument in the first index of the queue.
  */
  const queue = new Array(person);

  /*
  - While there are people in the queue, the first person will be processed in the loop.
    - The current person's friends array will be filtered to remove friends that have already been processed by checking
      for presence in the visited Set.
    - If the friend has not been visited, the "friend's" object is retrieved from the people_pool.json
        - For each friend, iterate through the movies array.
            - If the movie.title key is not present in the movieOccurences object, the movie.title key
                is added to the movieOccurences object with a value of 1.
            - If the movie.title key is present in the movieOccurences object, the movie.title key is incremented by one.
            - Iteration Continues...
        - After the current friend's movies are processed, the current friend is added to the visited set and pushed to
            the end of the queue
        - Iteration Continues...
    - After the currentPerson's friends are processed, the currentPerson is shifted from the front of the queue.
    - Iteration Continues...
  - Once the entire queue has been processed ( The entire specified network )
  - The most popular movie in the movieOccurences object (key(s) with the highest value)
    is found by sorting the movies (keys) by their number of occurences (values).
  - A string is returned stating the most popular movie(s) and the number of occurences in the selected network.
  */

  while (queue.length) {
    let currentPerson = queue[0];

    currentPerson.network
      .filter((friend) => !visited.has(friend))
      .forEach((friend) => {
        friend = peoplePool.find((person) => friend === person.name);

        friend?.movies.forEach((movie) => {
          if (!movie) return;
          let title = movie["title"];

          if (!movieOccurrences[title]) {
            movieOccurrences[title] = 1;
          } else {
            movieOccurrences[title] += 1;
          }
        });
        visited.add(friend.name);
        queue.push(friend);
      });
    queue.shift();
  }

  let sortedMovies = Object.entries(movieOccurrences)
    .sort((a, b) => a[1] - b[1])
    .reverse();

  // Filter out all keys with values less than the max;
  sortedMovies = sortedMovies
    .filter((movie, i, sortedMovies) => movie[1] === sortedMovies[0][1])
    .sort();

  return sortedMovies.length === 1
    ? `The most popular movie in ${person.name}'s network is ${sortedMovies[0][0]}. Found ${sortedMovies[0][1]} times.`
    : `The most popular movies in ${
        person.name
      }'s network are ${sortedMovies.map((movie) => movie[0])}. Each found ${
        sortedMovies[0][1]
      } times.`;
};

module.exports = getMostPopMovie;
