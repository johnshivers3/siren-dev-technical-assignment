const chai = require("chai");
const { it } = require("mocha");
const assert = chai.assert;
const getMostPopMovie = require("../index");

// Person node constructor
/*
class Person {
  constructor(name, email, network, movies) {
    this.name = name;
    this.email = email;
    this.network = network;
    this.movies = movies;
  }
} */
// Test
describe("Return most popular movie(s) in given network", function () {
  it(`should return the most popular movie in given person's network`, function () {
    let test = {
      name: "Yvette Braun PhD",
      email: "Braxton89@hotmail.com",
      network: [
        "Preston Gislason",
        "Randal Upton",
        "Julie Runolfsson",
        "Troy Tromp",
        "Daryl Luettgen",
      ],
      movies: [
        {
          title: "Once Upon a Time in America",
          rank: "78",
          id: "tt0087843",
        },
        { title: "Citizen Kane", rank: "48", id: "tt0033467" },
        { title: "Goodfellas", rank: "15", id: "tt0099685" },
      ],
    };
    assert.equal(
      getMostPopMovie(test),
      "The most popular movie in Yvette Braun PhD's network is The Prestige. Found 5 times."
    );
  });
});
