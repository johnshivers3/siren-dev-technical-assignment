const top100 = require("./top100.json");
const faker = require("faker");
const fs = require("fs");

class Person {
  constructor(name, email) {
    this.name = name;
    this.email = email;
    this.network = [];
    this.movies = Array(3);
  }
}

const peoplePool = (() => {
  let pool = [];
  let i = 50;
  while (i > 0) {
    let newPerson = new Person(faker.name.findName(), faker.internet.email());
    pool.push(newPerson);
    i--;
  }
  return pool;
})();

//Randomly movies from the Top 100 list and keep a count for comparison against func return value

const addMovies = (person) => {
  let i = 2;
  while (i >= 0) {
    let index = +(Math.random() * 100).toFixed(0);
    let currentMovie = top100[index];
    person.movies[i] = currentMovie;

    i--;
  }
  return person;
};

// Add friends to  network
const addFriends = (person) => {
  let randomNumber = Math.ceil(Math.random() * 10);
  let network = Array(randomNumber);
  let i = network.length;

  while (i > 0) {
    let newFriend =
      peoplePool[Math.ceil(Math.random() * peoplePool.length - 1)];
    if (newFriend["name"] === person["name"]) continue;
    if (!network.find((friend) => friend?.name === newFriend.name)) {
      network.push(newFriend.name);
      network.shift();
    }
    i--;
  }
  person.network = network;
  return person;
};

const withNet = peoplePool.map((person) => addFriends(person));

const withMovies = withNet.map((person) => addMovies(person));

fs.writeFile("./people_pool.json", JSON.stringify(withMovies), (err) => {
  if (err) {
    console.error(err);
    return;
  }
});

const allMovies = peoplePool
  .map((person) => ({
    name: person.name,
    movies: [
      ...person.movies.map((movie) => (movie?.title ? movie.title : null)),
    ],
  }))
  .sort((a, b) => a.name - b.name);

fs.writeFile("./movie_pool.json", JSON.stringify(allMovies), (err) => {
  if (err) {
    console.error(err);
    return;
  }
});
