Siren technical interview - Coding assignment

**Problem:**

    At Facebook, you have friends who all have a list of movies that they like.
    Write a function that returns the most popular movie of a person in his/her network of friends.
    Network means that you have to consider friends of friends as well.

**Solution:**

    Use object oriented design with the language you like. Write at least 1 test for the happy path.

    Implementation of a breadth-first search, accounting for all movies present in each node of the graph representation of the friend network.
    Returns a statement noting the most common movie in the network or movies if there is a tie.

**run:** 

    npm install

**test:**

    npm test

**Person Node**

    class Person {
        constructor(name, email) {
        this.name = name;
        this.email = email;
        this.network = [];
        this.movies = Array(3);
        }
    }
    

